# Bilan 2020

[[_TOC_]]

## Général

```mermaid
gantt
    title Catégories
    dateFormat DDDD
    axisFormat  
    todayMarker off
    
    Livres - 19 :a, 1, 19d
    Series - 6 :b, 1, 6d
    Mangas - 29 :c, 1, 29d
    Jeux-video - 7 :d, 1, 7d
    Films - 33 :e, 1, 33d
    Comics - 25 :f,1,25d
    Nouvelles - 4 :g,1,4d
    BD - 2 :h,1,2d
```

Après plusieurs années à essayer de noter ma consommation de culture, j'ai enfin trouvé une méthode qui me convient. 
Un simple document sur google docs accessible depuis mon PC et mon Téléphone. Le but... Faire des statistiques et un
bilan. Pour ce début d'année 2021, je peux enfin m'essayer à l'écriture d'un bilan pour 2020.

Dans les statistiques deux séries que j'apprécie ne seront pas présentes:
- **[Lovecraft Country](https://www.allocine.fr/series/ficheserie_gen_cserie=22139.html)** :  
Pas encore fini, je n'arrive pas à me mettre dans l'ambiance.
- **[The Queen's Gambit](https://www.allocine.fr/series/ficheserie_gen_cserie=24971.html)** :  
Pas encore fini, mais j'adore et je n'ai pas envie que la série se termine...

Après plusieurs années le nez dans les mangas, j'ai entamé un virage vers les comics depuis 2019. Plusieurs oeuvres
m'ont marqué **[Locke & key](https://hicomics.fr/catalogue/9782378870782-bienvenue-a-lovecraft/)**, **[Tony Chu - Détective cannibale](https://www.bedetheque.com/serie-25408-BD-Tony-Chu-Detective-cannibale.html)** et bien d'autres. Pour les mangas, je sélectionne mieux mes lectures et je ne dévore plus tout ce qui sort ou passe entre mes mains.

Par rapport à 2019, j'ai dégagé plus de temps libre pour la lecture de romans, et c'était vraiment génial. Maintenant
j'ai toujours un livre en cours de lecture...

## Les Livres

Livres lus: 19  
Pages lues: 10379  
Meilleure note: A  
Pire note: D  
Notes: A (11); B (6); C (1); D (1)  

### Les déceptions

- **[Secret Show](https://www.babelio.com/livres/Barker-Secret-show/19445)** de *Clive Barker* [Note: C]:  
Aucun souvenir du livre avant la lecture de la quatrième de couverture. La première partie m'avait complètement accroché, la suite a été pénible et longue.
- **[Les 3 p'tits cochons - Les contes interdits](https://www.babelio.com/livres/Boivin-Les-3-ptits-cochons/997310)** de *Christian Boivin* [Note: D]:  
Au détour de recherche pour remplir ma pile à lire... Je suis tombé sur des commentaires plus ou moins élogieux sur la série des "Contes interdits"... les quelque 240 pages sont inintéressantes, on y parle de cul entrecoupé de scènes de violence, le tout enrobé d'une histoire navrante.


### Les coups de cœur

- **[La voie des rois](https://www.babelio.com/livres/Sanderson-Les-Archives-de-Roshar-tome-1--La-Voie-des-rois-1/943770)** de *Brandon Sanderson* [Note: A]:  
Après le lobby plus ou moins intensif et discret, parler régulièrement de *brandon sanderson* et de ses séries, recevoir en cadeau une anthologie de ses nouvelles... J'ai commencé par deux nouvelles de science-fiction vraiment excellentes (**Parfait État**,**Snapshot**). La découverte de l'univers de **La voie des rois** se fait petit à petit. Les personnages sont vraiment bien écrits, les différentes trames sont excellentes. Une très bonne lecture.
- **[Seconde Fondation](https://www.babelio.com/livres/Asimov-Le-Cycle-de-Fondation-tome-3--Seconde-Fondation/6286)** de *Isaac Asimov* [Note: A]:  
C'est mon tome préféré du cycle de la fondation. Il relance avec brio l'intrigue.
- **[Running Man](https://www.babelio.com/livres/King-Running-man/4419)** de *Stephen King* [Note: A]:  
Publié sous le nom de plume *Richard Bachman*, j'ai adoré ce roman d'anticipation. Le livre est plutôt court, mais développe rapidement son univers, on y croit et on se laisse porter par les scènes d'action.

## Les séries

Séries vues: 6  
Épisodes: 47  
heures: ~34  
Meilleure note: A  
Pire note: B  
Notes: A (4), B (2)  

### Les déceptions

Actuellement, je note que les séries terminées. Les séries ont trois épisodes pour m'intéresser, si ce n'est pas le cas j'arrête le visionnage.
Pour l'année prochaine, je pense inscrire chaque épisode d'une série.

### Les coups de cœur

- **[Upload](https://www.allocine.fr/series/ficheserie_gen_cserie=22769.html)** [Note: A]:  
Conseillée par ma soeur, cette série humoristique m'a bien fait rire. La surprise du premier épisode est vraiment excellente !

## Les mangas

Tomes lus: 29  
Meilleure note: A  
Pire note: C  
Notes: A (18), B (9), C (2)  

### Les déceptions

- **[World War Demons](https://www.manga-news.com/index.php/serie/World-War-Demons)** de *Okabe Urû* [Note moyenne: C][11 Tomes]:  
La série débute très bien, mais elle s'enferme très vite dans un schéma classique au point où les 3 derniers tomes ont été durs à lire... Dommage le pitch était très prometteur.

### Les coups de cœur

- **[Happy !](https://www.manga-news.com/index.php/serie/Happy-Deluxe)** de *Naoki Urasawa* [Note moyenne: A][15 Tomes]:  
Enfin Panini s'est sorti les doigts, en février 2020 l'éditeur à réimprimer les tomes de la série. À part quelques tomes un peu en dessous, globalement la série est très intéressante, le personnage principal est vraiment touchant. J'ai eu beaucoup de mal à lâcher les volumes, au point où je me suis couché très tard pour finir les derniers tomes.

## Les jeux vidéos

Jeux terminés: 7  
Heures: 247   
Meilleure note: A++++  
Pire note: B  
Notes: A++++ (1), A(4), B(2)  

### Les déceptions

- **[The last of us](https://www.jeuxvideo.com/jeux/playstation-3-ps3/00042999-the-last-of-us.htm)** [Note: B]:  
Je me suis laissé attirer par les trailers de **[The last of us part II](https://www.jeuxvideo.com/test/1237466/the-last-of-us-part-ii-la-vengeance-au-coeur-d-un-recit-epoustouflant.htm)**. J'avais plusieurs possibilités pour jouer au premier opus; Ressortir la PS3, attendre qu'on me prête une PS4 ou essayer la plateforme PS Now... J'ai fait le pire choix, la plateforme Ps Now. J'ai eu quelques soucis avec la manette Xbox, j'ai dû jouer à la manette PS3. Niveau gameplay le jeu est rigide, les phases de fusillades étaient une purge. Je n'ai pas aimé le personnage principale.

### Les coups de cœur

- **[Hollow Knight](https://www.jeuxvideo.com/jeux/jeu-616488/)** [Note: A++++]:
En début d'année j'ai fini **[Ori and the Will of Wisps](https://www.jeuxvideo.com/jeux/jeu-669795/)** qui m'a laissé un peu sur ma faim. Cherchant un autre jeu type plateforme à faire, un ami m'a conseillé **[Hollow Knight](https://www.jeuxvideo.com/jeux/jeu-616488/)**. Attendant son heure tranquillement dans ma bibliothèque steam depuis 2017, j'ai donc lancé le jeu par défaut. 70h plus tard... le jeu est beau, l'univers est incroyablement riche, le gameplay est fabuleux.... Une vraie pépite ! J'attends la suite avec plus d'impatience qu'une annonce d'un futur **[Deus Ex](https://www.jeuxvideo.com/test/525976/deus-ex-mankind-divided-notre-verdict-sur-la-reference-du-fps-rpg-sci-fi.htm)**. 
- **[Hades](https://www.jeuxvideo.com/jeux/jeu-970357/)** [Note: A]:  
Pendant quelques jours j'ai eu les yeux rivés sur les streamers qui jouaient au jeu. Puis un ami me l'a offert et le temps a disparu. Le jeu est vraiment extra. 

## Les films

Films vus: 33  
Meilleure note: A  
Pire note: D  
Notes: A (9), B (13), C (7), D (4)

### Les déceptions

- **[Origines secrètes](https://www.allocine.fr/film/fichefilm_gen_cfilm=235753.html)** de *David Galàn Galindo* [Note: D]:  
Petite soirée tranquille avec ma compagne, on tombe d'accord sur ce film... Une grosse erreur, aucun de nous n'a apprécié le film, c'est long, c'est cliché.
- **[Color out of space](https://www.allocine.fr/film/fichefilm_gen_cfilm=271206.html)** de *Richard Stanley* [Note: D]:  
La promesse était intéressante, l'exécution n'était pas terrible.

### Les coups de cœur

- **[Jojo Rabbit](https://www.allocine.fr/film/fichefilm_gen_cfilm=258998.html)** de *Taika Waititi* [Note: A]:  
Comment rigoler devant un drame ? Le film s'en sort super bien, on rit, on pleure... Un super film !
- **[Les figures de l'ombre](https://www.allocine.fr/film/fichefilm_gen_cfilm=219070.html)** de *Theodore Melfi* [Note: A]:  
Choisi par ma compagne, le film nous a marqués tous les deux. Une belle découverte.
- **[Akira](https://www.allocine.fr/film/fichefilm_gen_cfilm=6429.html)** de *Katsuhiro Ôtomo* [Note: A]:  
Ressorti en version 4K dans nos salles de cinéma, j'en ai profité pour aller le voir 2 fois. Une première fois en VF, qui reste perfectible, puis en VO. J'aime tout dans ce film, l'histoire, les graphismes, l'animation.

## Les Comics

Comics lus: 25  
Meilleure note: A  
Pire note: B  
Notes: A (14), B (11)  

### Les décéptions

- **[Les nouvelles aventures de sabrina](https://www.glenat.com/log/sabrina-9782344036242)** [Note: B]:  
Après la série netflix, j'ai voulu tester le comics. L'histoire est intéressante, mais graphiquement ce n'était pas vraiment à mon gout. Une toute petite déception !

### Les coups de cœur

- **[Middlewest TP#1](https://imagecomics.com/comics/releases/middlewest-book-1-tp)** [Note: A]:  
Le comics m'a marqué par ses graphismes somptueux, mais l'histoire est aussi très prenante.
- **[La grande épopée de Picsou vol.1](https://www.glenat.com/les-grands-maitres/la-grande-epopee-de-picsou-tome-01-9782723491655)** [Note: A]:  
Le livre en lui-même est extraordinaire. Les commentaires de l'auteur sont complets. À chaque nouveau chapitre, on se surprend à chercher la signature "DUCK" caché par l'auteur.

