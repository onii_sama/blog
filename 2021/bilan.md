# Bilan 2021

[[_TOC_]]

## Général

```mermaid
gantt
    title Catégories
    dateFormat DDDD
    axisFormat  
    todayMarker off
    
    Livres - 15 :a, 1, 15d
    Series - 14 :b, 1, 14d
    Mangas - 29 :c, 1, 29d
    Jeux-video - 2 :d, 1, 2d
    Films - 38 :e, 1, 38d
    Comics - 18 :f,1,18d
    BD - 5 :h,1,5d
```
